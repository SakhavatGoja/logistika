import React from 'react'

const XInput = (props) => {
  const {label,id,className,placeholder, style, type,onChange,value,defaultValue,required,error} = props;
  return (
    <>
      {label && <label> { label } </label>}
      <input 
        type={type || "text"}
        id={id}
        className = {className || ""}
        style = {style}
        placeholder = {placeholder}
        defaultValue = {defaultValue}
        value = {value}
        onChange = {onChange}
        required={required}
      />
      {error && <div style = {{color:"red"}}>{error}</div>}
    </>
  )
}

export default XInput
