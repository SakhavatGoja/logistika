import React from 'react'
import LogoWhite from '../../img/logistika_white_logo.svg';
import leftIconUtilts from '../../themes/utilts/leftIcon-utilts';
import { Link } from 'react-router-dom';


const Xleft = ({title , imgStatusCode}) => {
  return (
    <div className="left_reg_log_container">
        <div className="left_btns">
          <Link to='/' className="heading_left">
            <img src={LogoWhite} alt="" />
          </Link>
          <button>
            <img src="/img/left_reg.svg" alt="" />
            geri qayıt
          </button>
        </div>
        <div className="left_center">
          <p className="center_title">{title}</p>
          {leftIconUtilts(imgStatusCode)}
        </div>
        <img src="/img/left_map.png" alt="" className='left_map'/>
    </div>
  )
}

export default Xleft