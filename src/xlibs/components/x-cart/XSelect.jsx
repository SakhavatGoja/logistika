import React ,{useState} from 'react'
import { useEffect } from 'react';
import { First } from 'react-bootstrap/esm/PageItem';

const XSelect = ({items}) => {
  console.log(items)
  const [showItems , setShowItems] = useState(false);
  const [first , setFirst] = useState({flag: '' , phone_code:''});
  // const [options ,setOptions] = useState({})

  const dropDown = () => {
    setShowItems(!showItems)
  };
  const selectItem = item => {
    setFirst(item);
  };
  
  // useEffect(()=>{
  //   async function getFirst (){
  //     let allItems = await items;
  //     let arr = await allItems[0];
  //     console.log(allItems)
  //     setFirst(prev => ({
  //       ...prev,
  //       flag: arr.flag,
  //       phone_code : arr.phone_code
  //     }))
  //   }
  //   getFirst();
  // },[])

  console.log(first)

  return (
    <div className="select-box--box">
      <div className="select-box--container" onClick={dropDown}>
        <div className="select-box--selected-item" >
            <img src={first.flag} alt="" />
            <p className="prefix">{first.phone_code}</p>
        </div>
        
        <div className={`select-box--arrow ${showItems ? 'up' :'down'}`} >
          <img src="/img/down.svg" alt="" />
        </div>

        <div
          className={`select-box--items ${showItems ? 'active' : null}`}
        >
          {
            items.map((item)=>(
            <div 
              key={item.id}
              onClick={() => selectItem(item)}
              className={`${item}` === item ? "selected" : ""}
            >
              <img src={item.flag} alt="" />
              <p className='prefix'>{item.phone_code}</p>
            </div>
            ))
          }
        </div>
      </div>
    </div>
  )
}

export default XSelect