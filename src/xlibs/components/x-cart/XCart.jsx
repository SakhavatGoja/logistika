import React from 'react'

const XCart = (props) => {
  const {Adornment ,text, title, strtext,className ,src } = props;
  return (
    <div className={className || "cart-right"}>
      <p className="title-text">{title && title}</p>
      {strtext && <p className="first-text">{strtext && strtext}</p>}
      <p className="second-text">{text && text}</p>
      {src && <div className="img-avatar">
        <img src={src} alt="" />
      </div>}
      {Adornment && Adornment}
    </div>
  )
}

export default XCart;