import React from 'react'

const XRadioCart = (props) => {
  const {value ,name ,required} = props;
  return (
    <label className='radio-form-group input-same'>
      {value}
      <input
        type='radio'
        name={name}
        required={required}
        />
    </label>
  )
}

export default XRadioCart