import React from 'react'
import { Modal } from 'react-bootstrap';


const XModal = (props) => {
  const {title ,data} = props;
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className='modal_list'
    >
      <button onClick={props.onHide} className='close'><img src="/img/x.svg" alt="" /></button>
      <div className="modal_header">{title}</div>
      <div className="modal_body">
        {
          data.map((item,id)=>(
            <label key={id}>
              {item.name}
              <input type="radio" name={'name'} id={id} />
            </label>
          ))
        }
      </div>
    </Modal>
  )
}

export default XModal