import React,{useState} from 'react'

const XFile = (props) => {
  const {name,placeholder , id} = props;
  const [img , setImg] = useState('');
  const handleAddBanner = ({ target: { files } }) => {
    setImg(files[0].name);
  };
  
  return (
    <div className="form-group">
      <div className="input-container input-same">
        <span>*</span>
        <label onChange={handleAddBanner} htmlFor={id} className='label-file'>
            {!img ? placeholder : img}
            <input
              name={name} 
              type="file" 
              id={id}
              accept="image/*"
              className='input-file' 
              required/>
        </label>  
      </div>
    </div>
  )
}

export default XFile