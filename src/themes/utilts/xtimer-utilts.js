import React , {useState , useEffect} from 'react'

const Timer = (props) => {
  const {initMinute , initSeconds} = props;
  const [minutes ,setMinutes] = useState(parseInt(initMinute));
  const [seconds ,setSeconds] = useState(parseInt(initSeconds));

  useEffect(() => {
    let myInterval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1)
      } if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(myInterval)
        } else {
          setMinutes(minutes - 1)
          setSeconds(59)
        }
      }
    }, 1000)
    return () => {
      clearInterval(myInterval)
    }
  })

  return (
    <>
			<div className='wrapper'>
				{ minutes === 0 && seconds === 0 ? (
					<>
            <div className="cardBody">
              <p>Vaxtınız doldu</p>
            </div>
					</>
				) : (
					<>
						<p>{minutes < 10 ? `0${minutes}` : minutes}:{seconds < 10 ? `0${seconds}` : seconds}</p>
					</>
				)}
			</div>
    </>
  )
}

export default Timer