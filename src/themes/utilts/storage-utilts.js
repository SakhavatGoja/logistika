export const setDataToStorage = (key, value)=>{
  localStorage.setItem(key ,value);
}
export const getDataFromStorage = (key)=>{
  return localStorage.getItem(key);
}

export const storageData =(key)=>{
  let initialState = {};
  if(getDataFromStorage(key)){
    try {
      initialState = {...JSON.parse(getDataFromStorage)}
    } catch (error) {
      console.log(error)
    }
  }
  return initialState
}

