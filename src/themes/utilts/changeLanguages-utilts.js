import {setDataToStorage} from './storage-utilts';
// import {changeLangBackEnd} from '../api/apiCall';


export const changeLanguage = (lang , i18n)=>{
  i18n.changeLanguage(lang);
  setDataToStorage('lng' , lang);
  // changeLangBackEnd(lang); ---- backend request
}