import React from 'react'

const statusCode = (e) => {
  switch (e) {
    case 1:
      return (
        <>
          <img src="/img/waiting.svg" alt="" />
          <p>Sifariş gözlədilir</p>
        </>
      )
    case 2:
      return (
        <>
          <img src="/img/done.svg" alt="" />
          <p>Sifariş catdirilib</p>
        </>
    )
    case 3:
      return (
          <>
            <img src="/img/done.svg" alt="" />
            <p>Sifariş icradadir</p>
          </>
    )
    case 4:
      return (
          <>
            <img src="/img/waiting.svg" alt="" />
            <p>Sifariş legv olunub</p>
          </>
    )
  
    default:
      break;
  }
}

export default statusCode