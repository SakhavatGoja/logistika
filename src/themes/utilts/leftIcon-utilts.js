import React from 'react'

const leftIconUtilts = (e) => {
  switch (e) {
    case 1:
      return <img src="/img/left_1_icon.svg" alt="" />
      
    case 2:
      return <img src="/img/left_2_icon.svg" alt="" />

    case 3:
      return <img src="/img/left_3_icon.svg" alt="" />

    case 4:
      return <img src="/img/left_4_icon.svg" alt="" />

    case 5:
      return <img src="/img/left_5_icon.svg" alt="" />

    default:
      break;
  }
}

export default leftIconUtilts