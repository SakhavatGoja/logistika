import React , {useState , useEffect} from 'react'
import { Tab, Tabs } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import XCart from '../../../xlibs/components/x-cart/XCart'
import XModal from '../../../xlibs/components/x-cart/XModal'
import XSelect from '../../../xlibs/components/x-cart/XSelect'
import XInput from '../../../xlibs/components/XInput'
import Data from '../../../json/data.json'
import {getAllCountry, signUp} from '../../../api/apiCall'
import { useDispatch } from 'react-redux'

const initialState = {
  key: '', 
  title: '',
  loadPop:{
      titleSeller: "Satıcılarınızın siyahısı",
      titleBroker: "Brokerlərinizin siyahısı",
      data :[]
  }
}

const initialUser = {
  number : '',
  password: ''
}

const Registration = ({eventKey}) => {
  const [state, setState] = useState(initialState);
  const [modalShow, setModalShow] = useState(false);
  const [user, setUser] = useState(initialUser);
  const [allCountries,setCountries] =useState([]);
  const [options, setoptions] = useState([]);

  useDispatch()

  useEffect(()=>{
    setState(prevState=>({
      ...prevState,
      key : eventKey
    }) );
  },[eventKey])

  useEffect(()=> { 
   getAllCountry()
   .then(res=>setCountries(res.data.data))
  },[])
  

  const changeUser = (e)=>{
    setUser({
      ...user,
      [e.target.name] : e.target.value
    })
  }

  const popUp = (e)=>{
    e.preventDefault();
    setModalShow(true);
    setState(prevState=>({
      ...prevState , 
      title: Data.loadPop.titleSeller
    }));
  }

  const setSignUP =()=>{
    signUp({
      number: user.number,
      password: user.password
    })
    .then((res)=>{
      console.log(res);
      setModalShow({
        ...state,
        loadPop : {
          ...state.loadPop,
          data: [
            ...state.loadPop.data,
            res.data
          ]
        }
      })
    })
    .catch(error=>console.log(error))
  }

  
  return (
    <>
      <XCart
        strtext='Xoş gəldin'
        text='Giriş etmək üçün telefon nömrənizi daxil edin'
        Adornment = {
            (
              <div className='first_step'>
                <Tabs
                  defaultActiveKey="home"
                  transition={false}
                  activeKey={state.key}
                  onSelect={(k) => setState({
                    ...state,
                    key: k
                  })}
                >
                  <Tab eventKey="login" title="Giriş">
                    <form className='standart_form form_first' onSubmit={(e)=>popUp(e)}>
                      <XInput
                        type="number"
                        className="input-same"
                        name='number'
                        defaultValue=""
                        placeholder="Nömrənizi daxil edin"
                        onChange={(e)=>changeUser(e)}
                        required
                      />
                      <XInput
                        type="number"
                        name="password"
                        className="input-same"
                        defaultValue=""
                        placeholder="Şifrəniz"
                        onChange={changeUser}
                      />
                      <button
                        type="submit"
                        className="input-submit"
                        onClick={setSignUP}
                        >
                         Giriş et
                      </button>
                    </form>
                    <Link to='/' className='forget'>Şifrəni unutdum</Link>
                  </Tab>
                  <Tab eventKey="registration" title="Qeydiyyat">
                    <form className='standart_form form_second'>
                      <div className="form-group input-same">
                        <div className="input-container">
                          <XSelect
                            items={allCountries}
                          />
                          <XInput
                            type="number"
                            className=""
                            defaultValue=""
                            placeholder="Nomrenizi daxil edin"
                          />  
                        </div>
                      </div>
                      <XInput
                        type="submit"
                        className="input-submit"
                        value="Qeydiyyatdan keç"
                      />
                    </form>
                  </Tab>
                </Tabs>
              </div>
            )
          }
      />
      <XModal 
        title ={state.title} 
        data={state.loadPop.data}
        show={modalShow}
        onHide={() => setModalShow(!modalShow)}
      />
    </>
  )
}

export default Registration