import React , {useState} from 'react'
import Registration from './registration/Registration'
import { useParams } from 'react-router-dom';
import Xleft from '../../xlibs/components/Xleft';


const Registered = () => {
  const [code , setCode] = useState(2);
  const params = useParams();
  return (
    <div className='reg_log_container'>
      <Xleft 
        title='Elan yerləşdir və özünün təyin etdiyiyi qiymətə yükünü daşıd'
        imgStatusCode={code}
      />
      <Registration eventKey={params.id}/>
    </div>
  )
}

export default Registered