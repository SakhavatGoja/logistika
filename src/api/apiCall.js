import axios from "axios";
const baseURL = 'http://pony.bcptest.online'
export const signUp = (data) => {
  return axios.post('url' , data)
}

export const loginAuth = (credentials) => {
  return axios.post('url' , null, {credentials})
}

export const changeLangBackEnd = (language) => {
  axios.defaults.headers['accept-language'] = language;
}

export const getAccount = ()=>{
  return axios.get('url')
}

export const getAllCountry = ()=>{
  return axios.get(`${baseURL}/api/v1/countries`)
}
