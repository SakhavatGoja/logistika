import { createStore } from "redux";
import { authReducer } from "./authReducer";
import { storageData } from "../themes/utilts/storage-utilts";

export const configStore =()=>{
  const initialState = storageData('globalState');
  // const initialState = {};
  const state = createStore(
    authReducer,
    initialState,
    );

  return state
}

export const store = configStore();
//store = {}