import React , {useState} from 'react';
import {BrowserRouter as Router , Routes ,Route ,Outlet } from 'react-router-dom';
import Header from './components/header';
import Footer from './components/footer'
import HomePage from './components/homepage';
import Guest from './components/guest';
import Principle from './components/principle';
import Contact from './components/contact';
import Order from './components/order';
import NotFound from './components/notFound';
import Notification from './components/notification';
import NotificationInner from './components/notificationInner';
import Profile from './components/profile'
import Registration from './components/Registration';
import CarAdd from './components/CarAdd';
import Registered from './pages/registered/Registered';
import CheckAccount from './components/checkAccount'
import AccountData from './components/accountData';
import ChangeNumber from './components/ChangeNumber';
import OTP from './components/OTP';
import ChangePassword from './components/ChangePassword';
import {loginAuth, signUp} from './api/apiCall'
import './App.css';
import 'swiper/css';
import 'swiper/css/navigation';



function App() {
  const params = "userlst";
 
  return (
   <>
    {
     params === "userlist" ? <LogReg/> : <Default/> 
    }
   </>
  );
}
function Default(){
  return(
    <Router>
      <div className="App">
      <Header/>
        <Routes>
            <Route  path='/' element={<HomePage/>} ></Route>
            <Route  path='home' element={<HomePage/>} ></Route>
            <Route  path='guest' element={<Guest/>} ></Route>
            <Route  path='principle' element={<Principle/>} ></Route>
            <Route  path='contact' element={<Contact/>} ></Route>
            <Route  path='order/*' element={<Order/>} ></Route>
            <Route  path='/notification' element={<Notification/>}></Route>
            <Route  path='/profile' element={<Profile/>} ></Route>
            <Route  path='/checkAccount' element={<CheckAccount/>} ></Route>
            <Route  path='/changeNumber' element={<ChangeNumber/>} ></Route>
            <Route  path='/changePassword' element={<ChangePassword/>} ></Route>
            <Route  path='/carAdd' element={<CarAdd/>} ></Route>
            <Route  path='/otp' element={<OTP/>} ></Route>
            <Route  path='/account-data/:id' element={<AccountData/>} ></Route>
            <Route  path={'registred/:id'} element={<Registered />}></Route>
            <Route  path='/notification/1' element={<NotificationInner/>}></Route>
            <Route  path='/registration-new' element={<Registration/>}></Route>
            <Route  path='*' element={<NotFound/>} ></Route>
        </Routes>
        <Footer/>
      </div>
    </Router>
  )
}

function LogReg(){
  return(
    <Router>
      <Routes>
        <Route  path='/registration' element={<Registration/>} ></Route>
      </Routes>
    </Router>
  )
}


export default App;
