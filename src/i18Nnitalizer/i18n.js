import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import {getDataFromStorage} from '../themes/utilts/storage-utilts'

i18n.use(initReactI18next).init({
  resources: {
    en: {
      translations: {
        'Sign Up': 'Sign Up',
        Username: 'Eldar',
        "title": "asdasdasdasd",
        required : 'Please fill out this field',
        max : 'Must be 15 characters or less',
        carPlaceHolder: 'Car plate',
        carModelPlaceholder: 'Model of the car',
        carTypesPlaceholder: 'Type of car',
        carSizesPlaceholder: 'Dimensions of the machine'
      }
    },
    az: {
      translations: {
        'Sign Up': 'Giriş et',
        Username: 'Elxan',
        "title": "Xiyar-pomidorun daşınması daşınması ...",
        carPlaceHolder: 'Maşın nömrəsi',
        carModelPlaceholder: 'Maşının modeli',
        carTypesPlaceholder: 'Maşının növü',
        carSizesPlaceholder: 'Maşının ölçüləri'
      }
    },
    ru: {
      translations: {
        'Sign Up': 'sdfsdfsdf',
        Username: 'Aleksandr',
        "title": "Xwertwertwertwertması ...",
        required : 'Пожалуйста, заполните это поле',
        max : 'ПожалуПожалуПожалуПожалу',
        carPlaceHolder: "Aвтомобильный номер",
        carModelPlaceholder: 'Модель автомобиля',
        carTypesPlaceholder: 'Тип автомобиля',
        carSizesPlaceholder: 'Размеры машины'
      }
    }
  },
  fallbackLng: getDataFromStorage('lng') || 'en',
  ns: ['translations'],
  defaultNS: 'translations',
  keySeparator: false,
  interpolation: {
    escapeValue: false,
    formatSeparator: ','
  },
  react: {
    wait: true
  }
});

export default i18n;