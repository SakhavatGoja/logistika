import React , {useState} from 'react'
import { Link } from 'react-router-dom';
import { Accordion, Card, useAccordionButton } from "react-bootstrap"
import { Swiper, SwiperSlide } from 'swiper/react';
import Data from '../json/data.json'
import truckBig from '../img/truckBig.png';
import cityMap from '../img/map.png'
import bigRoad from '../img/bigRoad.png'
import truck2 from '../img/truck2.png';
import googlePlay from '../img/play.jpg'
import appstore from '../img/store.jpg'
import startLoc from '../img/startLoc.svg'
import finishLoc from '../img/finishLoc.svg'

import 'bootstrap/dist/css/bootstrap.min.css';


function CustomToggle({ children, eventKey, handleClick }) {
  const decoratedOnClick = useAccordionButton(eventKey, () => {
    handleClick();
  });
  return (
    <div className="card-header" type="button" onClick={decoratedOnClick}>
      {children}
    </div>
  );
}

export default function HomePage() {
  const [activeKey, setActiveKey] = useState(null);
  const [show, setShow] = useState(false);

  return (
    <div className='indexPage d-none'>
      <div className="landing_page">
        <div className="landing_1400">
          <div className="container">
            <div className="row">
              <div className="inner_container">
                <div className="inner_landing">
                  <div className="head_left">
                    <p>Biz hər şeyin öhdəsindən gələ bilərik.</p>
                    <h2>Böyük və ya kiçik...</h2>
                  </div>
                  <p className='inner_content'>
                    Etibar edə biləcəyiniz avadanlıq.
                    Etibar edə biləcəyiniz insanlar
                  </p>
                  <div className="foot_left">
                    <p>Mobil tətbiqimizi yükləyin.</p>
                    <div>
                      <Link to='/home'>
                        <img src={googlePlay} alt="" />
                      </Link>
                      <Link to='/home'>
                        <img src={appstore} alt="" />
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <img src={truckBig} alt="" className='truckBig'/>
        </div>
      </div>
      <div className="index_about">
        <div className="index_part_1_1400">
          <div className="city_map">
            <img src={cityMap} alt="" />
          </div>
          <div className="container">
            <div className="row">
              <p className="about_title">Biz nə edirik ?</p>
            </div>
          </div>
          <div className="road_box">
            <div className="big_road">
              <img src={bigRoad} alt="" />
            </div>
            {
              Data.indexAbout.map((item ,id)=>(
                <Link to='/login' className="about_single_data" key={id}>
                  <div className="about_data_left">
                    <div></div>
                  </div>
                  <div className="about_data_right">
                    <p className="about_data_title">{item.title}</p>
                    <p className="about_data_content">{item.content}</p>
                  </div>
                </Link>
              ))
            }
          </div>
        </div>

        <div className="index_part_2_1400">
          <div className="container">
            <div className="row">
              <div className="about_content_box">
                <div className="left_ellipse_start">
                  <div className="inner_ellipse">
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M29.0903 11.0036C29.0013 10.7413 28.8374 10.5109 28.6188 10.3407C28.4003 10.1705 28.1367 10.068 27.8607 10.0459L19.7842 9.40419L16.2893 1.66777C16.178 1.4186 15.997 1.20696 15.7681 1.0584C15.5391 0.909845 15.2721 0.830716 14.9992 0.830567C14.7263 0.830417 14.4592 0.909252 14.2301 1.05756C14.001 1.20586 13.8197 1.4173 13.7082 1.66635L10.2132 9.40419L2.13682 10.0459C1.86546 10.0674 1.60605 10.1667 1.3896 10.3317C1.17316 10.4968 1.00886 10.7207 0.91635 10.9767C0.823842 11.2327 0.807047 11.51 0.867974 11.7753C0.928901 12.0406 1.06497 12.2827 1.2599 12.4727L7.22832 18.2909L5.11749 27.4313C5.05339 27.7079 5.07393 27.9975 5.17645 28.2623C5.27896 28.5272 5.45871 28.7551 5.69237 28.9166C5.92603 29.078 6.20282 29.1655 6.48681 29.1676C6.7708 29.1698 7.04889 29.0865 7.28498 28.9287L14.9987 23.7862L22.7125 28.9287C22.9538 29.0889 23.2384 29.1714 23.528 29.165C23.8175 29.1587 24.0982 29.0638 24.3323 28.8932C24.5663 28.7225 24.7425 28.4843 24.8372 28.2105C24.9318 27.9368 24.9403 27.6406 24.8616 27.3619L22.2705 18.2952L28.6965 12.5124C29.1172 12.1327 29.2717 11.5405 29.0903 11.0036Z" fill="#353A3F"/>
                    </svg>
                  </div>
                </div>
                <div className="inner_content_box">
                  {
                    Data.truck.map((item ,id)=>(
                      <div className="about_row" id={id} key={id}>
                        <span>
                          <svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.14427 3.82459C9.11808 3.74745 9.06987 3.67967 9.0056 3.62961C8.94133 3.57956 8.86381 3.54942 8.7826 3.54292L6.40719 3.35417L5.37927 1.07876C5.34653 1.00547 5.29329 0.943223 5.22595 0.89953C5.15862 0.855837 5.08009 0.832564 4.99982 0.83252C4.91956 0.832476 4.84099 0.855662 4.77361 0.899282C4.70623 0.942901 4.65292 1.00509 4.6201 1.07834L3.59219 3.35417L1.21677 3.54292C1.13696 3.54925 1.06066 3.57843 0.997 3.62698C0.93334 3.67553 0.885016 3.74139 0.857808 3.81668C0.830599 3.89198 0.82566 3.97352 0.843579 4.05155C0.861499 4.12958 0.901519 4.20079 0.958852 4.25667L2.71427 5.96792L2.09344 8.65626C2.07458 8.73763 2.08063 8.82279 2.11078 8.90069C2.14093 8.97859 2.1938 9.04563 2.26252 9.0931C2.33124 9.14058 2.41265 9.16631 2.49618 9.16694C2.57971 9.16758 2.6615 9.1431 2.73094 9.09667L4.99969 7.58417L7.26844 9.09667C7.33941 9.1438 7.42311 9.16805 7.50828 9.16619C7.59345 9.16432 7.67601 9.13641 7.74485 9.08622C7.81369 9.03604 7.86551 8.96597 7.89334 8.88545C7.92117 8.80493 7.92367 8.71782 7.90052 8.63584L7.13844 5.96917L9.02844 4.26834C9.15219 4.15667 9.1976 3.98251 9.14427 3.82459Z" fill="#F79824"/>
                          </svg>
                        </span>
                        <p>{item.text}</p>
                      </div>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>
          <div className="truck_2">
            <img src={truck2} alt="" />
          </div>
        </div>

      </div>
      <div className="index_swiper">
        <div className="container">
          <div className="row">
            <p className="index_swiper_title">Sürücülər üçün yeni elanlar</p>
          </div>
        </div>
        <div className="swiper-mock">
          <Swiper
            loop={true}
            spaceBetween={46}
            slidesPerView={3}
            centeredSlides={true}
            centeredSlidesBounds={true}
            >
            {
              Data.swiper.map((item ,id)=>(
                <SwiperSlide key={id}>
                  <Link to='/login'>
                    <div className="swiper_inner_container">
                      <div className="swiper_head">
                        <p className="text-clamp">{item.title}</p>
                        <p className="text-date">{item.date}</p>
                      </div>
                      <div className="swiper_body">
                        <div className="swiper_body_inner">
                          <div className="loc_box">
                            <img src={startLoc} alt="" />
                            <div className="line"></div>
                            <img src={finishLoc} alt="" />
                          </div>
                          <div className="position_box">
                            <div className="start_pos_box same_pos_box">
                              <p className="start_city same_city">{item.start.city}</p>
                              <p className="start_place same_place">{item.start.place}</p>
                            </div>
                            <div className="distance">
                              {item.distance}
                              <div className="timeline"></div>
                            </div>
                            <div className="finish_pos_box same_pos_box">
                              <p className="finish_city same_city">{item.finish.city}</p>
                              <p className="finish_place same_place">{item.finish.place}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                </SwiperSlide>
              ))
            }
          </Swiper>
        </div>
      </div>
      <div className="index_accordion">
        <p className="accordion_index_title">Yaranacaq problemlərlə bağlı suallara cavab</p>
        <Accordion defaultActiveKey={0} activeKey={activeKey}>
          {Data.indexAccordion.map((item , id)=>(
             <Card key={id}>
              <CustomToggle
                as={Card.Header}
                eventKey={id}
                handleClick={() => {
                  if (activeKey === id) {
                    setActiveKey(null);
                  } else {
                    setActiveKey(id);
                  }
                }}
              >
                {item.title}
                <span>{activeKey === id ? "-" : "+"}</span>
              </CustomToggle>
             <Accordion.Collapse eventKey={id}>
               <Card.Body>{item.text}</Card.Body>
             </Accordion.Collapse>
           </Card>
          ))}
        </Accordion>
      </div>
    </div>
  )
}
