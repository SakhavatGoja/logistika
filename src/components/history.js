import React , {useState} from 'react'
import SimpleBar from 'simplebar-react';
import DatePicker from 'react-datepicker';
import Select from 'react-select';
import Data from '../json/data.json'
import statusCode from '../themes/utilts/statusCode-utilts';
import useStyles from './styleComponent/styleSimplebar';

import "react-datepicker/dist/react-datepicker.css";





const History = () => {
  const classes = useStyles();
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const options = [
    {
      label : 'all',
      value : 'hamsi'
    },
    {
      label : 'waiting',
      value : 'Gozleyen'
    },
    {
      label : 'delivered',
      value : 'Catdirilib'
    },
    {
      label : 'inProcess',
      value : 'Icradadir'
    },
    {
      label : 'canceled',
      value : 'legv olunub'
    },
  ]

  return (
    <SimpleBar className={classes.simplebar}>
      <div className='history'>
        <p className="history_title">Sifariş tarixçəsi</p>
        <div className="history_container">
          <div className="history_box">
            {
              Data.history.map((item,id)=>(
                <div className="history-item" key={id}>
                  <div className="item_top">
                    <p className='item-text text-clamp'>{item.text}</p>
                    <p className='item-date text-date'>{item.date}</p>
                  </div>
                  <div className="item_bottom">
                    <p className="text-price">{item.price} <span><img src="/img/manat.svg" alt="" /></span></p>
                    <div className="status_code">{statusCode(item.status)}</div>
                  </div>
                </div>
              ))
            }
          </div>
          <form action="" className="history_search_form">

            <label className='calendarLabel' htmlFor="startDate">
              Başlanğıc
              <div className="form-input-group form">
                  <DatePicker
                    placeholderText="gg/aa/ii"
                    selected={startDate}
                    onChange={(date) => setStartDate(date)}
                    dateFormat="dd/MM/yyyy"
                    selectsStart
                    startDate={startDate}
                    endDate={endDate}
                    id='startDate'
                  />
                </div>
            </label>

            <label className='calendarLabel' htmlFor="endDate">
              Bitiş
              <div className="form-input-group form">
                <DatePicker
                  placeholderText="gg/aa/ii"
                  selected={endDate}
                  onChange={(date) => setEndDate(date)}
                  dateFormat="dd/MM/yyyy"
                  selectsEnd
                  startDate={startDate}
                  endDate={endDate}
                  minDate={startDate}
                  id='endDate'
                />
              </div>
            </label>

            <label className='selectLabel'>
              <Select
                placeholder='ugg'
                options={options}
              />
            </label>

            <label className="buttonLabel">
              <button type="submit" className='white_blue'>
                Axtarış et
              </button>
            </label>
          </form>
        </div>
      </div>
    </SimpleBar>
  )
}

export default History