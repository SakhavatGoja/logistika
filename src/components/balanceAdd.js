import React ,{useState} from 'react'
import { Tab, Tabs } from 'react-bootstrap'
import { Accordion, Card, useAccordionButton } from "react-bootstrap"
import SimpleBar from 'simplebar-react';
import useStyles from './styleComponent/styleSimplebar';
import Data from '../json/data.json'


const initialState = {
  key: '',
  total: 0,
  activeKey: null
}

const prices= [10, 12, 14, 16, 18]
let sum = 0;

function CustomToggle({ children, eventKey, handleClick }) {
  const decoratedOnClick = useAccordionButton(eventKey, () => {
    handleClick();
  });
  return (
    <div className="card-header" type="button" onClick={decoratedOnClick}>
      {children}
    </div>
  );
}

export default function BalanceAdd() {
  const classes = useStyles();
  const [state, setState] = useState(initialState);

  const activeHandle =(e)=>{
    e.preventDefault();
    const price = parseInt(e.target.getAttribute('data-price'));
    sum += price;
    setState({
      ...state,
      total: sum
    })
  }

  const decrease = () =>{
    if(state.total<1){
      return 0
    }
    setState({
      ...state,
      total: state.total-1
    });
  }
  
  const increase = () =>{
    setState({
      ...state,
      total: state.total+1
    });
  }

  return (
    <SimpleBar className={classes.simplebar}>
      <div className="history balance_add">
        <p className="history_title">Balansı artırmaq üçün ödəmə üsulunu seçin</p>
          <div className='balance_operation'>
            <p className="level-text"> 1. Mərhələ </p>
            <Tabs
              defaultActiveKey="home"
              transition={true}
              activeKey={state.key}
              onSelect={(k) => setState({
                ...state,
                key: k
              })}
            >
              <Tab 
                eventKey="login" 
                title={<div><img src="/img/bycart.svg" alt="" /> Kartla </div>}
              >
                <p className="level-text"> 2. Mərhələ </p>
                <div className="money_tab">
                  <div className="btn-prices">
                    {
                      prices.map((item,id)=>(
                        <button
                          key={id} 
                          id={id}
                          data-price={item} 
                          onClick={(e)=>activeHandle(e)}>
                            {item} azn
                        </button>
                      ))
                    }
                  </div>
                  <div className="result_box">
                    <p className="title-result">Məbləği daxil et</p>
                    <div className="result_price">{state.total}azn</div>
                    <div className="operation_btns">
                      <button onClick={()=>decrease()}><img src="/img/minus.svg" alt="" /></button>
                      <button onClick={()=>increase()}><img src="/img/plus.svg" alt="" /></button>
                    </div>
                    <button className="white_blue submit-balance" disabled={state.total<5? true : false} >Balansı artır</button>
                  </div>
                </div>
              </Tab>
              <Tab 
                eventKey="registration" 
                title={<div><img src="/img/byterminal.svg" alt="" /> Terminalla </div>}
                >
                <p className="level-text"> 2. Mərhələ </p>
                <Accordion defaultActiveKey={0} activeKey={state.activeKey}>
                  {Data.balance.map((item , id)=>(
                    <Card key={id}>
                      <CustomToggle
                        as={Card.Header}
                        eventKey={id}
                        handleClick={() => {
                          if (state.activeKey === id) {
                            setState({
                              ...state,
                              activeKey: null
                            });
                          } else {
                            setState({
                              ...state,
                              activeKey: id
                            });
                          }
                        }}
                      >
                        {item.title}
                        <img className={state.activeKey === id?'active':null} src='/img/right_accordion.svg' alt=''/>
                      </CustomToggle>
                    <Accordion.Collapse eventKey={id}>
                      <Card.Body>
                        <div className="banknote-box">
                          {
                            item.data.map((item,id)=>(
                              <div className="banknote-item" key={id} > 
                                <img src={item} alt="" />
                                <p className="number-img">
                                  {id+1}.
                                </p>
                              </div>
                            ))
                          }
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  ))}
                </Accordion>
              </Tab>
            </Tabs>
          </div>
      </div>
    </SimpleBar>
  )
}
