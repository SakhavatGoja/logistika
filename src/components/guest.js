import React , {useState} from 'react';
import { Link } from 'react-router-dom';
import Accordion from './accordion';
import Data from '../json/data.json'
// import GoogleMapReact from 'google-map-react';
import SwiperCore, { Navigation} from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import SimpleBar from 'simplebar-react';
import plus from '../img/plus.svg';
import road_left from '../img/road_left.png';
import startLoc from '../img/startLoc.svg'
import finishLoc from '../img/finishLoc.svg'
import next_arrow from '../img/next_arrow.svg'
import person from '../img/person.svg'
import { useTranslation } from "react-i18next";
import useStyles from './styleComponent/styleSimplebar';

import 'simplebar/dist/simplebar.min.css';


SwiperCore.use([Navigation]);

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const defaultProps = {
  center: {
    lat: 40.409264,
    lng: 49.867092
  },
  zoom: 11
};

export default function Guest() {
  const {t} = useTranslation();
  const [expanded, setExpanded] = useState('panel0');
  const [number ,setNumber] = useState(5);

  const classes = useStyles();

  const handleChange = (panel) => (e,newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const navigationPrevRef = React.useRef(null)
  const navigationNextRef = React.useRef(null)

  return (
    <div className="guest_container">
      <SimpleBar className={`${classes.simplebar} left-simplebar`}>
        <div className="guest_left">
          <div className="order_left_box left_same_box">
            <p className="order_left_title">Siz sifarişçisiniz ?</p>
            <p className="order_left_body">
              Ən yaxın daşıyıcıları və ya avadanlıq sahiblərini birbaşa dispetçer olmadan axtarmaq üçün sifariş verin.
            </p>
            <button>
              Sifariş ver
              <img src={plus} alt="" />
            </button>
          </div>
          <div className="client_left_box left_same_box">
            <p className="order_left_title">Müştəri axtarırsınız ?</p>
            <p className="order_left_body">
              Vasitəsiz və sizə yaxın olan avadanlıqlarınız üçün iş tapmaq üçün elan əlavə edin.
            </p>
            <button>
              Texnika əlavə et
              <img src={plus} alt="" />
            </button>
          </div>
        </div>
      </SimpleBar>
      <SimpleBar className={classes.simplebar}>
        <div className="guest_right">
          <div className="map" >
            {/* <GoogleMapReact
              bootstrapURLKeys={{ key: 'AIzaSyA4SgjsTpN_D4nuu-O2Dn8f5aeK7dLjwoQ' }}
              defaultCenter={defaultProps.center}
              defaultZoom={defaultProps.zoom}
            >
              <AnyReactComponent
                lat={40.409264}
                lng={49.867092}
                text="ay gedee"
              />
            </GoogleMapReact> */}
          </div>
          <p className="map_desc">
            <span>*</span>
            Xəritə üzərində mövcud sürücülər və sifarişçilər qeyd olunmuşdur. Daha detallı məlumat əldə
             etmək üçün qeydiyyatdan keçin və ya daxil olun
          </p>
          <div className="swiper-mock">
            <p className="same-title-guest">Elanlar</p>
            <Swiper
              modules={[Navigation]}
              spaceBetween={20}
              slidesPerView={2.4}
              navigation={{
                prevEl: navigationPrevRef.current,
                nextEl: navigationNextRef.current,
              }}
              onBeforeInit={
                (swiper) => {
                      swiper.params.navigation.prevEl = navigationPrevRef.current;
                      swiper.params.navigation.nextEl = navigationNextRef.current;
                }
              }
              >
              {
                Data.swiper.map((item ,id)=>(
                  <SwiperSlide key={id}>
                    <Link to='/login'>
                      <div className="swiper_inner_container">
                        <div className="swiper_head">
                          <p className="text-clamp">{t("Username")}</p>
                          <p className="text-date">{item.date}</p>
                        </div>
                        <div className="swiper_body">
                          <div className="swiper_body_inner">
                            <div className="loc_box">
                              <img src={startLoc} alt="" />
                              <div className="line"></div>
                              <img src={finishLoc} alt="" />
                            </div>
                            <div className="position_box">
                              <div className="start_pos_box same_pos_box">
                                <p className="start_city same_city">{item.start.city}</p>
                                <p className="start_place same_place">{item.start.place}</p>
                              </div>
                              <div className="distance">
                                {item.distance}
                                <div className="timeline"></div>
                              </div>
                              <div className="finish_pos_box same_pos_box">
                                <p className="finish_city same_city">{item.finish.city}</p>
                                <p className="finish_place same_place">{item.finish.place}</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </SwiperSlide>
                ))
              }
              <div className="custom_navigation">
                <button className="prev_btn" ref={navigationPrevRef} ><img src={next_arrow} alt="" /></button>
                <button className="next_btn" ref={navigationNextRef} ><img src={next_arrow} alt="" /></button>
              </div>
            </Swiper>
            </div>
          <div className='accordion'>
            <p className="same-title-guest">Necə kömək edə bilərik ?</p>
            {Data.accordionData.map((item ,id)=>(
              <Accordion key={id} title={item.title} content={item.content} index={id} expanded ={expanded} handleChange={handleChange(`panel${id+1}`)}/>
            ))}
          </div>
          <div className="hot_line">
            <div className="img_left"><img src={person} alt="" /></div>
            <div className="hot_right">
              <p> Probleminiz həll olunmadığı halda bizə yazın </p>
              <button>Bizə yazın</button>
            </div>
          </div>
        </div>
      </SimpleBar>
    </div>
  );
}
