import React from 'react'
import { Link } from 'react-router-dom';
import breadcrumb from '../img/breadcrump.svg'

export default function Principle(props) {
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="breadcrumbs">
            <Link to='/'>MERlogistics</Link>
            <img src={breadcrumb} alt="" />
            <span>İşləmə prinsipi</span>
          </div>
          <div className="main_principle_data">
            <div className="head_principle">
              <h1>İşləmə prinsipi</h1>
              <p>Updated and effective: <span className="principle_date">October, 2021</span></p>
            </div>
            <div className="body_principle">
              <p className='body_block'>
                Welcome to Resso, the social music streaming App, is provided or controlled by Moon Video Inc. ("we" or "us").

                We are committed to protecting and respecting your privacy. This policy explains our practices concerning the personal data we collect from you, or that you provide to us.

                Please refer to the “jurisdiction specific” provisions for certain countries or regions set out at the end of the Privacy Policy.

                Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By accessing or using our Resso platform, website, services, applications, products and content made available on such platform(s) (collectively, the "Services"), you acknowledge that you have read this policy and that you understand your rights in relation to your personal data and how we will collect, use and disclose it.

                Capitalized terms that are not defined in this policy have the <a href="#">meaning given to them in the Terms of Services</a>.
              </p>
              <div className="block_body">
                <p className="head_block">Welcome to Resso</p>
                <p className="body_block">
                  the social music streaming App, is provided or controlled by Moon Video Inc. ("we" or "us").

                  We are committed to protecting and respecting your privacy. This policy explains our practices concerning the personal data we collect from you, or that you provide to us.
                </p>
              </div>
              <div className="block_body">
                <p className="head_block">Please refer to the</p>
                <p className="body_block">
                  “jurisdiction specific” provisions for certain countries or regions set out at the end of the Privacy Policy.

                  Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By accessing or using our Resso platform, website, services, applications, products and content made available on such platform(s) (collectively, the "Services"), you acknowledge that you have read this policy and that you understand your rights in relation to your personal data and how we will collect, use and disclose it.

                  Capitalized terms that are not defined in this policy have the <a href="#">meaning given to them in the Terms of Services</a>.
                </p>
              </div>
              <div className="block_list">
                <div className="list_row">
                  <div className="list_main"><p>Please read the following carefully to understand our views and practices regarding your personal data and how</p> </div>
                </div>
                <div className="list_row">
                  <div className="list_main"><p>Please read the following carefully to understand our views and practices regarding your personal data and how</p> </div>
                  <ul className="sublist">
                    <li>Please read the following carefully to understand our views and practices regarding your personal data and </li>
                    <li>how we Will treat it. By accessing or using our Resso platform, website, services, applications, products and </li>
                    <li>content made Available on such platform(s) (collectively, the "Services"), you acknowledge that you have read </li>
                    <li>content made Available on such platform(s) (collectively, the "Services"), you acknowledge that you have read </li>
                  </ul>
                </div>
                <div className="list_row">
                  <div className="list_main"><p>Please read the following carefully to understand our views and practices regarding your personal data and how</p> </div>
                </div>
                <div className="list_row">
                  <div className="list_main"><p>Please read the following carefully to understand our views and practices regarding your personal data and how</p> </div>
                  <ul className="sublist">
                    <li>Please read the following carefully to understand our views and practices regarding your personal data and </li>
                    <li>how we Will treat it. By accessing or using our Resso platform, website, services, applications, products and </li>
                    <li>content made Available on such platform(s) (collectively, the "Services"), you acknowledge that you have read </li>
                    <li>content made Available on such platform(s) (collectively, the "Services"), you acknowledge that you have read </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
