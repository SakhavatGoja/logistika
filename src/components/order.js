import React from 'react'
import {BrowserRouter as Router , Routes ,Route   } from 'react-router-dom';
import LeftBar from './leftBar'
import Loads from './loads'
import Seller from './seller'
import Broker from './broker'
import InnerLoad from './innerLoad'
import Complain from './complain'
import DriverList from './driverList'
import Bid from './bid'
import MapContainer from './map'
import History from './history'
import Advertisement from './advertisements'
import BalanceHistory from './balanceHistory';
import Cars from './cars';
import BalanceAdd from './balanceAdd';
import FileDownload from './FileDownload';


export default function Order() {
  return (
    <div className='main_theme_container'>
      <LeftBar/>
      <Routes>
        <Route path='/' element={<Loads />}></Route>
        <Route path='/loads' element={<Loads />}></Route>
        <Route path='/loads/:id' element={<InnerLoad />}></Route>
        <Route path='/seller' element={<Seller />}></Route>
        <Route path='/broker' element={<Broker />}></Route>
        <Route  path='/complain' element={<Complain/>} ></Route>
        <Route  path='/driver' element={<DriverList/>} ></Route>
        <Route  path='/map' element={<MapContainer/>} ></Route>
        <Route  path='/bid' element={<Bid/>} ></Route>
        <Route  path='/history' element={<History/>} ></Route>
        <Route  path='/advertisement' element={<Advertisement/>} ></Route>
        <Route  path='/fileDownload' element={<FileDownload/>} ></Route>
        <Route  path='/balanceHistory' element={<BalanceHistory/>} ></Route>
        <Route  path='/balanceAdd' element={<BalanceAdd/>} ></Route>
        <Route  path='/cars' element={<Cars/>} ></Route>
      </Routes>
    </div>
  )
}
