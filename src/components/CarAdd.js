import React , {useState , useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import Xleft from '../xlibs/components/Xleft';
import XCart from '../xlibs/components/x-cart/XCart';
import XInput from '../xlibs/components/XInput';
import { useTranslation } from "react-i18next";
import {useFormik ,} from 'formik'
import * as Yup from 'yup'
import Select from 'react-select'
import CustomSelect from './CustomSelect';

const initialUser = {
  number : '',
  password: ''
}

const carSelect = {
  models:  [
    { value: 'mercedes', label: 'Mercedes' },
    { value: 'bmw', label: 'BMW' },
   ],
  types:  [
    { value: 'sedan', label: 'sedan' },
    { value: 'hundur', label: 'hundur' },
    { value: 'agir', label: 'agir' },
   ],
   sizes:  [
    { value: '4x4', label: '4x4' },
    { value: '5x5', label: '5x5' },
    { value: '6x6', label: '6x6' },
   ]
}

const styles = {
  select :{
    width: '100% !important'
  }
}

export default function CarAdd() {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const formik  = useFormik({
    initialValues : {
      car_number: "",
      models:'',
      types: '',
      sizes: ''
    },
    validationSchema : Yup.object({
      car_number : Yup.string().max(15, t("max")).required(t("required")),
      models : Yup.string().required(t('required')),
      types : Yup.string().required(t('required')),
      sizes : Yup.string().required(t('required')),
    }),
    onSubmit: (values)=>{
      console.log(values)
    }
  });
  return (
    <div className='reg_log_container'>
      <Xleft 
        title='Elan yerləşdir və özünün təyin etdiyiyi qiymətə yükünü daşıd'
        imgStatusCode={5}
      />
      <XCart
        strtext='Masin əlavə et'
        className='cart-right cart-car'
        Adornment = {
            (
              <form 
                  className='standart_form yellow_form' 
                  onSubmit={formik.handleSubmit} 
                  autoComplete="off"
                >
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <CustomSelect
                        className='custom-select w-100'
                        options={carSelect.models}
                        placeholder={t('carModelPlaceholder')}
                        name='models'
                        onChange={value=>formik.setFieldValue('models' , value.value)}
                      />
                    </div>
                    {formik.errors.models ? <span className='error-message'>{formik.errors.models}</span> : null }
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        id="car_number"
                        name="car_number"
                        className=""
                        placeholder={t('carPlaceHolder')}
                        onChange= {formik.handleChange}
                        value={formik.values.car_number}
                      />  
                    </div>
                    {formik.errors.car_number ? <span className='error-message'>{formik.errors.car_number}</span> : null }
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <CustomSelect
                        className='custom-select w-100'
                        options={carSelect.types}
                        placeholder={t('carTypesPlaceholder')}
                        name='types'
                        onChange={value=>formik.setFieldValue('types' , value.value)}
                      />
                    </div>
                    {formik.errors.types ? <span className='error-message'>{formik.errors.types}</span> : null }
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <CustomSelect
                        className='custom-select w-100'
                        options={carSelect.sizes}
                        placeholder={t('carSizesPlaceholder')}
                        name='sizes'
                        onChange={value=>formik.setFieldValue('sizes' , value.value)}
                      />
                    </div>
                    {formik.errors.sizes ? <span className='error-message'>{formik.errors.sizes}</span> : null }
                  </div>
                  <button
                    type="submit"
                    className="input-submit"
                    >
                      Hesabı yoxla
                  </button>
              </form>
            )
        }
      />
    </div>
  )
}
