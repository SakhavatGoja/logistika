import React , {useState} from 'react';
import SimpleBar from 'simplebar-react';
import useStyles from './styleComponent/styleSimplebar';
import { Link , useNavigate } from 'react-router-dom';

const displayData =[];
const objFile =[];
const fileArray =[];

export default function FileDownload() {
  const classes = useStyles();
  const [data , setData] = useState({showdata : displayData , postVal : '' , imageFile : []});
  const navigate = useNavigate();


  let obj = [
    {
      user_id:1,
      message:'salam',
      type:'text'
    },
  ]

  const onChange =(e)=>{
    const value = e.target.value !== '' ? e.target.value : '';
    setData({
      ...data ,
      postVal : value
    })
  }

  const handleImageInput = (e) => {
    objFile.push(e.target.files)
    for (let i = 0; i < objFile[0].length; i++) {
        fileArray.push(URL.createObjectURL(objFile[0][i]));
    }
    setData({
      ...data ,
      imageFile: fileArray
    })
  };

  const messageSubmit =(e)=>{
    e.preventDefault();
    data.postVal && displayData.push(data.postVal);
    setData({
      ...data,
      showdata : displayData,
      postVal : ''
    })
  }

  return (
  
    <div className="notification_main_container">
      <div className="head_notification">
        <button onClick={()=> navigate(-1)}>
          <img src="/img/left_breadcrump.svg" alt="" />
        </button>
        <p className="title_not">Sənəd yüklə</p>
      </div>
       
      <div className="download-chat-ui h-100">
          <SimpleBar className={classes.simplebar}>
            <div className="chat-body">
              <div className="chat-left chat-box">
                <div className="user-message-box">
                  {
                    displayData.length > 0
                      ? 
                      <div className="profile-avatar">
                        <p>Nicat Alverdiyev</p>
                        <div className="avatar-img">
                          <img src="/img/driver.svg" alt="" />  
                        </div>
                      </div>
                    : null
                  }
                  {
                    displayData.map((item,id)=>(
                      <p key={id} className='message-text'>{item}</p>
                    ))
                  }
                </div>
              </div>
              <div className="chat-right chat-box">
                <div className="user-message-box">
                  {
                    displayData.length > 0
                      ? 
                      <div className="profile-avatar">
                        <p>Nicat Alverdiyev</p>
                        <div className="avatar-img">
                          <img src="/img/driver.svg" alt="" />  
                        </div>
                      </div>
                    : null
                  }
                  {
                    displayData.map((item,id)=>(
                      <p key={id} className='message-text'>{item}</p>
                    ))
                  }
                </div>
              </div>
            </div>
          </SimpleBar>
          <div className="chat-footer">
            <form className='message-form' onSubmit={e=>messageSubmit(e)}>
              <input 
                type="text"
                value={data.postVal}
                onChange={e=>onChange(e)}
                className='input-message' 
                placeholder='Mesaj...'
              />
              <div className="form-chat-right">
                <label onChange={handleImageInput} htmlFor='input-file' className='label-file'>
                  <img src="/img/photographer.svg" alt="" />
                  <input
                    name='input-file'
                    type="file" 
                    id='input-file'
                    accept="image/*"
                    className='input-file'
                    multiple="multiple" 
                  />
                </label>
                <label onChange={handleImageInput} htmlFor='input-file' className='label-file'>
                  <img src="/img/pscript.svg" alt="" />
                  <input
                    name='input-file'
                    type="file" 
                    id='input-file'
                    accept="image/*"
                    className='input-file'
                    multiple="multiple" 
                  />
                </label>
                <button type="submit">
                    <img src="/img/message.svg" alt="" />
                </button>
              </div>
            </form>  
          </div>
      </div>
    </div>
  )
}
