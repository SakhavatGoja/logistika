import React , {useState} from 'react'
import SimpleBar from 'simplebar-react';
import LeftBar from './leftBar';
import useStyles from './styleComponent/styleSimplebar';
import { Link } from 'react-router-dom';


export default function Profile() {
  const classes = useStyles();
  const [img ,setImg ] = useState('/img/driver.svg')
  const handleChange = (event) =>{
    let input = event.target;
    let reader = new FileReader();
    reader.onload = function(){
      let dataURL = reader.result;
      setImg(dataURL);
    };
    reader.readAsDataURL(input.files[0]);
  }
  return (
    <div className='main_theme_container'>
      <LeftBar/>
      <SimpleBar className={classes.simplebar}>
        <div className="profile">
          <form action="">
            <label htmlFor='image_upload' className="image-label">
              <div className="profile_img">
                <img src={img} alt="" />
                <div className='btn_img'><img src="/img/camera.svg" alt="" /></div>
              </div>
              <input type="file" name="image_upload" id="image_upload" onChange={handleChange}/>
            </label>
            <label htmlFor="country">
              Ölkə
              <input type="text" name='country' id='country' value='Azərbaycan' disabled/>
            </label>
            <label htmlFor="phone" className='phone-label'>
              Telefon nömrəniz
              <input type="text" name='phone' id='phone' value='+994514374946' disabled/>
              <Link to='/changeNumber'><img src="/img/pen.svg" alt="" /></Link>
            </label>
            <label htmlFor="fullName">
              Ad Soyad
              <input type="text" name='fullName' id='fullName' value='Nicat Alverdiyev' disabled/>
            </label>
            <label htmlFor="company">
              Şirkət adı
              <input type="text" name='company' id='company' value='Baku creative projects' disabled/>
            </label>
            <label htmlFor="mail">
              E-poçt
              <input type="email" name='mail' id='mail' value='Firengiznuriyeva@gmail.com' disabled/>
            </label>
            <label htmlFor="voen">
              VÖEN
              <input type="number" name='voen' id='voen' value='1523945867' disabled/>
            </label>
            <label htmlFor="">
              <Link to='/changePassword' className='changePassword'>
              Şifrəni dəyiş
              </Link>
            </label>
          </form>
        </div>
      </SimpleBar>
    </div>
  )
}
