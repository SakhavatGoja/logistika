import React , {useState , useEffect} from 'react'
import {useNavigate, useParams} from 'react-router-dom';
import SimpleBar from 'simplebar-react';
import Xleft from '../xlibs/components/Xleft';
import XCart from '../xlibs/components/x-cart/XCart';
import XInput from '../xlibs/components/XInput';
import useStyles from './styleComponent/styleSimplebar';

export default function AccountData() {
  const classes = useStyles();
  return (
    <div className='reg_log_container'>
      <Xleft 
        title='Elan yerləşdir və özünün təyin etdiyiyi qiymətə yükünü daşıd'
        imgStatusCode={1}
      />
      <SimpleBar className={classes.simplebar} >
        <div className="account-container w-100">
          <XCart
            src='/img/avatar1.svg'
            className="cart-right cart-account"
            Adornment = {
              (
                <div className='account-check-box'>
                    <div className="account-heading">
                      <div className="text-account">
                        <p className="name">Elşən Məmmədov</p>
                        <p className="status">Məşğul</p>
                      </div>
                      <div className="acc-img">
                        <img src="/img/greece.svg" alt="" />
                      </div>
                    </div>
                    <button
                      type="submit"
                      className="input-submit"
                      >
                        Əlavə et
                    </button>
                </div>
              )
            }
          />
          <XCart
              title="Broker əlavə et"
              className={'cart-right right-same-form-container'}
              Adornment = {
                (
                  <form  className='standart_form third_form yellow_form'>
                    <div className="form-group input-same">
                      <span>*</span>
                      <select name="country" id="country" className='form-select'>
                        <option value="1">Azerbaijan</option>
                        <option value="2">Russia</option>
                        <option value="3">USA</option>
                        <option value="4">Georgia</option>
                        <option value="5">Sweden</option>
                      </select>
                    </div>
                    <div className="form-group input-same">
                      <span>*</span>
                      <XInput
                        type="number"
                        defaultValue=""
                        placeholder="Telefon nömrəniz"
                        required
                      />
                    </div>
                    <div className="form-group input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Ad Soyad"
                        required
                      />
                    </div>
                    <div className="form-group input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Şirkət adı"
                        required
                      />
                    </div>
                    <div className="form-group input-same">
                      <span>*</span>
                      <XInput
                        type="email"
                        defaultValue=""
                        placeholder="E-poçt"
                        required
                      />
                    </div>
                    <div className="form-group input-same">
                      <span>*</span>
                      <XInput
                        type="number"
                        defaultValue=""
                        placeholder="VÖEN"
                        required
                      />
                    </div>
                    <div className="form-group input-same">
                      <span>*</span>
                      <XInput
                        type="password"
                        defaultValue=""
                        placeholder="Şifrə "
                        required
                      />
                    </div>
                    <div className="form-group input-same">
                      <span>*</span>
                      <XInput
                        type="password"
                        defaultValue=""
                        placeholder="Şifrəni təkrarla"
                        required
                      />
                    </div>
                    <div className="form-group input-same">
                      <span>*</span>
                      <XInput
                        type="password"
                        defaultValue=""
                        placeholder="Şifrəni təkrarla"
                        required
                      />
                    </div>
                    <XInput
                      className='input-submit input-submit-2'
                      type="submit"
                      value="Qeydiyyatı bitir"
                    />
                  </form>
                )
              }
            />

        </div>
     </SimpleBar>
    </div>
  )
}
