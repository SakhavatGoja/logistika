import React from 'react';
import Select from 'react-select'

const CustomSelect=({onChange ,name , options , value  , className , placeholder}) => {
  const defaultValue = (options ,value)=>{
    return options ? options.find(option=>option.value ===value) : ""
  }
  return ( 
    <Select
      className={className}
      name={name}
      placeholder={placeholder}
      value={defaultValue(options ,value)}
      onChange={onChange}
      options={options}
    />
  )
}
export default CustomSelect;
