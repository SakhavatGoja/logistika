import React from 'react'
import SimpleBar from 'simplebar-react';
import {Link, useNavigate } from 'react-router-dom';
import useStyles from './styleComponent/styleSimplebar';

export default function Complain() {
  const classes = useStyles();
  const navigate = useNavigate();

  return (
    <SimpleBar className={classes.simplebar}>
      <div className="notification_main_container">
        <div className="head_notification">
          <button onClick={()=> navigate(-1)}>
            <img src="/img/left_breadcrump.svg" alt="" />
          </button>
          <p className="title_not">Sürücünü şikayət et</p>
        </div>
        <div className="body_form">
          <p className='form_text'>
          Gedişlə bağlı rast gəlinən problem səbəbinizi seçin və ya ətraflı şəkildə öz şikayətinizi qeyd edin.
          </p>
          <form action="" className='complainForm'>
            <div className="radio_box">
              <label htmlFor="different">
                Sürücü fərqli yoldan istifadə etdi
                <input type="radio" name="complain" id="different" required/>
              </label>
              <label htmlFor="blunt">
                Sürücü kobud idi
                <input type="radio" name="complain" id="blunt" required/>
              </label>
              <label htmlFor="others">
                Gediş baş tutmadı
                <input type="radio" name="complain" id="others" required/>
              </label>
              <label htmlFor="highPrice">
                Nəzərdə tutulandan daha yüksək məbləğ istədi
                <input type="radio" name="complain" id="highPrice" required/>
              </label>
            </div>
            <label className="textarea_label">
            Şikayətinizi qeyd edin
            <textarea name="" id="" cols="30" rows="10"></textarea>
            </label>
            <button type="submit" class="btn_blue">Göndər</button>
          </form>
        </div>
      </div>
    </SimpleBar>
  )
}
