import React ,{useState} from 'react'
import SimpleBar from 'simplebar-react';
import { Link } from 'react-router-dom'
import useStyles from './styleComponent/styleSimplebar';
import Data from '../json/data.json'
import loc from '../img/loc.svg'
import XModal from '../xlibs/components/x-cart/XModal';


const loads = true;
const type = 0;
export default function Loads() {
  const classes = useStyles();
  const [modalShow, setModalShow] = useState(false);
  const [title, setTitle] = useState('');

  const popUp = (e)=>{
    const data_type = parseInt(e.target.getAttribute('data-type'));
    data_type === 1 ? setTitle(Data.loadPop.titleSeller) : setTitle(Data.loadPop.titleBroker);
    setModalShow(true);
  }

  return (
    <SimpleBar className={classes.simplebar}>
      <div className='main_container'>
        {
          loads ? 
                <div className="loads">
                  <p className="main_title">Mövcud yüklər</p>
                  <div className="loads_list">
                    {
                      Data.loads.yes.map((item,id)=>(
                        <div className="load_row" key={id}>
                          <Link to='/order/loads/innerLoad' className="loads_box">
                            <div className="load_left">
                              <p className="loads_name">{item.text}</p>
                              <div className="loads_price_list">
                                <p className="price text-price">{item.price} <span><img src="/img/manat.svg" alt="" /></span></p>
                                <div className="status_code">
                                  {
                                    parseInt(item.statusCode) === 0 ? 
                                      <p><img src={Data.statusList[0].img} alt="" /> {Data.statusList[0].text}</p> :
                                      parseInt(item.statusCode) === 1 ?
                                      <p><img src={Data.statusList[1].img} alt="" /> {Data.statusList[1].text}</p>: 
                                      parseInt(item.statusCode) === 2 ?
                                      <p><img src={Data.statusList[2].img} alt="" /> {Data.statusList[2].text}</p>
                                        : null
                                  }
                                </div>
                              </div>
                            </div>
                            <div className="load_road">
                              <div className="date_distance">
                                <p className='date'>{item.date}</p>
                                <div className="distance_box">
                                  <div className="loc_img_box"><img src={loc} alt="" /></div>
                                  <p className="distance_text">{item.distance}</p>
                                  <div className="loc_img_box"><img src={loc} alt="" /></div>
                                </div>
                              </div>
                              <div className="road_map">
                                <div className="start_road same_road">
                                  <p className="city">{item.start.city}</p>
                                  <p className="country">({item.start.country})</p>
                                </div>
                                <div className="road_stroke"></div>
                                <div className="end_road same_road">
                                  <p className="city">{item.finish.city}</p>
                                  <p className="country">({item.finish.country})</p>
                                </div>
                              </div>
                            </div>
                            <img className='right_go_inner' src="/img/go_in.svg" alt="" />
                          </Link>
                            <div className="loads_btns">
                              {
                                type === 0
                                && 
                                <>
                                  <button 
                                    className='white_blue_2' 
                                    data-type='1' 
                                    onClick={(e)=>popUp(e)}
                                  >
                                    Satıcı əlavə edin
                                  </button>
                                  <button 
                                    className='white_blue_2' 
                                    data-type='2'
                                    onClick={(e)=>popUp(e)}
                                    >
                                      Broker əlavə edin
                                  </button>
                                </>
                              }
                              
                            </div>
                          
                        </div>
                      ))
                    }
                  </div>
                </div> 
                : <div className='none_broker'>
                    <p>{Data.loads.no[0].text}</p>
                    <button>{Data.loads.no[0].link}</button>
                  </div>
        }
      <XModal 
        title ={title} 
        data={Data.loadPop.data}
        show={modalShow}
        onHide={() => setModalShow(!modalShow)}
      />

      </div>
    </SimpleBar>
  )
}
