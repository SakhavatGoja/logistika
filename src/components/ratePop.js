import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { Rating } from 'react-simple-star-rating'
import { Modal } from 'react-bootstrap';


export default function MyCenterPopUp(props) {
  const [rating, setRating] = useState(0) 

  // Catch Rating value
  const handleRating = (number) => {
    setRating(number)
  }
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className='modal_rate'
    >
      <button onClick={props.onHide} className='close'><img src="/img/x.svg" alt="" /></button>
      <div className="modal_header">Sürücünü qiymətləndir</div>
      <div className="modal_body">
        <Rating 
          onClick={handleRating} 
          ratingValue={rating}
          fullIcon={<img src="/img/full_blue_star.svg" alt="full"/>}
          emptyIcon={<img src="/img/empty_star.svg" alt="empty"/>}
          />
        <Link to='/order/complain' className='complain'>Sürücünü şikayət et</Link>
      </div>
    </Modal>
  );
}
