import React from 'react'
import { Link } from 'react-router-dom';
import breadcrumb from '../img/breadcrump.svg'
import notFound from '../img/notFound.png'

export default function NotFound() {
  return (
    <div className=''>
      <div className="container">
        <div className="row">
          <div className="breadcrumbs">
            <Link to='/'>MERlogistics</Link>
            <img src={breadcrumb} alt="" />
            <span>İşləmə prinsipi</span>
          </div>
          <div className="notFound_box">
            <img src={notFound} alt="" />
            <p>Səhifə tapılmadı</p>
          </div>
        </div>
      </div>
    </div>
  )
}
