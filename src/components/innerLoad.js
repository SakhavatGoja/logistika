import React , {useState} from 'react';
import SimpleBar from 'simplebar-react';
import useStyles from './styleComponent/styleSimplebar';
import { Link } from 'react-router-dom';
import MyCenterPopUp from './ratePop';
import finishLoc from '../img/finishLoc.svg';

import Data from '../json/data.json';



const a = 1;

export default function InnerLoad() {
  const classes = useStyles();
  const [open ,setOpen] = useState(false);
  const [modalShow, setModalShow] = useState(false);

  const toggle = () => setOpen(!open)
  return (
  <SimpleBar className={classes.simplebar}>
    <div className="main_innerload_container">
        <div className="load_info_left">
          <div className="load_breadcrump">
            <Link to='/order/loads'>
              <img src="/img/left_breadcrump.svg" alt="" />
            </Link>
            <div className="load_status">
              <img src="/img/delivered.svg" alt="" />
              <p>Sifariş icradır</p>
            </div>
          </div>
          <div className="loads_inner">
            <p className="inner_heading">{Data.innerLoad.head}</p>
            <div className="loads_table">
              {
                Data.innerLoad.body.map((item,id)=>(
                  <div className="body_row" key={id}>
                    <p className="row_left">{item.head}</p>
                    <p className="row_right">{item.body}</p>
                  </div>
                ))
              }
            </div>
            {
              a === 1 ?
                <Link to='/order/fileDownload' className="download_link">
                Sənədləri yüklə
                  <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1.25033 10.6667L5.91699 6.00008L1.25032 1.33342" stroke="#09306D" strokeWidth="1.16667" strokeLinecap="round" strokeLinejoin="round"/>
                  </svg>
                </Link>
                : a === 3 ?
                    <div className="delivered_info_box">
                      <div className="delivered_broker delivered_same">
                        <p className="title">Brokeriniz:</p>
                        <p className="name">Nicat.A</p>
                      </div>
                      <div className="dashed_line"></div>
                      <div className="delivered_seller delivered_same">
                        <p className="title">Satıcınız:</p>
                        <p className="name">Nicat.A</p>
                      </div>
                    </div>
                    : null
            }

            <div className="load_map_box ">
              <div className="load_map_data">
                <div className="loc_box">
                  <img src={finishLoc} alt="" />
                  <div className="line"></div>
                  <img src={finishLoc} alt="" />
                </div>
                <div className="position_box">
                  <div className="start_pos_box same_pos_box">
                    <p className="start_city same_city">Bakı</p>
                    <p className="start_place same_place">Azərbaycan Respublikası, Bakı</p>
                  </div>
                  <div className="distance">
                    453 km
                    <div className="timeline"></div>
                  </div>
                  <div className="finish_pos_box same_pos_box">
                    <p className="finish_city same_city">Gəncə</p>
                    <p className="finish_place same_place">Azərbaycan Respublikası, Zaqatala</p>
                  </div>
                </div>
              </div>
              <Link to='/order/map' className="map_template ">
                <img src="/img/map_img.png" alt="" />
                <p>Xəritədə baxın</p>
              </Link>
            </div>


          </div>
        </div>

        {
            a === 1 ? 
              <div className="contact_load" onClick={()=>toggle()}>
                <div className={`other_btns ${open ? "show" : ""} `}>
                  <button>
                    <img src="/img/phone.svg" alt="" />
                  </button>
                </div>
                <button className='contact_btn'>
                  <img src="/img/truck.svg" alt="" />
                  <p>Əlaqə</p>
                </button>
              </div> 
              : null
        }

        {
          a === 1 ?
          <div className="submit_btns">
            <button className="btn_blue_border" onClick={() => setModalShow(true)}>Sifariş tamamlandı</button>
          </div>
          : a === 2 ?
            <div className="submit_btns">
              <Link to='/order/bid' className="btn_blue_border">Qiymət təkliflərinə bax</Link>
              <button className="btn_default">Sifarişi ləğv et</button>
            </div>
            : null
        }

      <MyCenterPopUp
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </div>
  </SimpleBar>
  )
}
