import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  simplebar: {
    maxHeight: '100%',
    overflowY: 'auto',
    width: '100%'
  }
});

export default useStyles;