import React from 'react'
import LeftBar from './leftBar'
import SimpleBar from 'simplebar-react';
import useStyles from './styleComponent/styleSimplebar';
import {Link, useNavigate } from 'react-router-dom';
import Data from '../json/data.json';




export default function Notification() {
  const classes = useStyles();
  const navigate = useNavigate();

  return (
    <div className='main_theme_container'>
      <LeftBar/>
      <SimpleBar className={classes.simplebar}>
        <div className='notification_main_container'>
          <div className="head_notification">
            <button onClick={()=> navigate(-1)}>
              <img src="/img/left_breadcrump.svg" alt="" />
            </button>
            <p className="title_not">Bildirişlər</p>
          </div>
          <div className="body_notification">
            {
              Data.notifications.map((item, id)=>(
                <div className="row_notification" key={id}>
                  <p className="date_not">{item.date}</p>
                  <div className="nt_table">
                    {item.data.map((row,id)=>(
                      <Link to='/notification/1' className="row_nt" key={id}>
                        <div className="left_nt">
                          <div className={`status_nt ${row.read ? "read" : "not_read"}`}></div>
                          <p className="text_nt">{row.text}</p>
                        </div>
                        <div className="right_nt">
                          <img src="/img/go_in.svg" alt="" />
                        </div>
                      </Link>
                    ))}
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </SimpleBar>
    </div>
  )
}
