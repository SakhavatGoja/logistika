import React ,{useState} from 'react'
import SimpleBar from 'simplebar-react';
import Data from '../json/data.json'
import useStyles from './styleComponent/styleSimplebar';


export default function BalanceHistory() {
  const classes = useStyles();
  

  return (
    <SimpleBar className={classes.simplebar}>
      <div className="history balance_history">
        <p className="history_title">Balansı tarixçəsi</p>
          {
            Data.balanceHistory.map((item,id)=>(
              <div className="balance_box" key={id}>
                <p className="item-date">{item.date}</p> 
                  {
                    item.data.map((row,id)=>(
                        <div className="balance_row" key={id}>
                          <div className="balance_img">
                            {row.type === 1 ? <img src='/img/balance1.svg' alt=''/> : <img src='/img/balance2.svg' alt=''/>}
                          </div>
                          <div className="balance_content">
                            <div className="balance_info">
                              <p className="payment_title payment-text">{row.title}</p>
                              <p className="payment_type">{row.payment_type}</p>
                            </div>
                            <div className="balance_payment_price">
                              <p className='payment-text'>
                                {
                                  row.type ===2 ? '-' : '+'
                                }
                                {row.payment_price}
                              </p>
                              <img src="/img/manat.svg" alt="" />
                            </div>
                          </div>
                        </div>
                    ))
                  }
                </div>
            ))
          }
        
      </div>
    </SimpleBar>
  )
}
