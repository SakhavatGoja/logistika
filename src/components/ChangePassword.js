import React , {useState , useEffect} from 'react'
import {useNavigate} from 'react-router-dom';
import Xleft from '../xlibs/components/Xleft';
import XCart from '../xlibs/components/x-cart/XCart';
import XInput from '../xlibs/components/XInput';
import XSelect from '../xlibs/components/x-cart/XSelect';
import {getAllCountry, signUp} from '../api/apiCall'


const initialUser = {
  number : '',
  password: ''
}


export default function ChangePassword() {
  const [user, setUser] = useState(initialUser);
  const navigate = useNavigate()
  
  return (
    <div className='reg_log_container'>
      <Xleft 
        title='Elan yerləşdir və özünün təyin etdiyiyi qiymətə yükünü daşıd'
        imgStatusCode={2}
      />
      <div className="container-right container-password w-100">
        <XCart
          strtext='Şifrəni dəyişdir'
          Adornment = {
              (
                <div className='first_step'>
                  <form className='standart_form form_first'>
                    <div className="form-group input-same">
                        <XInput
                          type="number"
                          className=""
                          defaultValue=""
                          placeholder="Nomrenizi daxil edin"
                          required
                        />
                      </div>
                      <div className="form-group input-same">
                        <XInput
                          type="password"
                          className=""
                          defaultValue=""
                          placeholder="Yeni şifrə"
                          required
                        />
                      </div>
                      <div className="form-group input-same">
                        <XInput
                          type="password"
                          className=""
                          defaultValue=""
                          placeholder="Yeni şifrəni təkrarla"
                          required
                        />
                      </div>
                      <button
                        type="submit"
                        className="input-submit"
                        >
                         Şifrəni dəyişdir
                      </button>
                  </form>
                </div>
              )
            }
        />
     </div>
    </div>
  )
}
