import React from 'react';
import SimpleBar from 'simplebar-react';
import { Link } from 'react-router-dom';
import Data from '../json/data.json'
import useStyles from './styleComponent/styleSimplebar';


export default function DriverList() {
  // const [rating, setRating] = useState(0);
  const classes = useStyles();
  return (
    <SimpleBar className={classes.simplebar}>
      <div className='main_container'>
        <div className="driver_box">
            <p className="main_title">Sürücü siyahısı</p>
            <div className="driver_list">
              {
               Data.driver.map((item,id)=>(
                 <div className="driver-item" key={id}>
                   <div className="driverImg">
                     <img src={item.img} alt="" />
                     </div>
                   <div className="driver-name-box same_grid">
                     <p className="driver-name text_grid">{item.name}</p>
                     <div className="stars_driver" id={item.star}>
                         {
                           [...Array(5)].map((e, i) =>{
                            const star = item.star;
                            return(
                              <span className={i<star ? 'active' : ''} key={i}></span>
                            )
                          })
                         }
                     </div>
                   </div>
                   <div className="weight_type same_grid">
                     <p className="car-weight text_grid">{item.weight}</p>
                     <p className="car-type">{item.type}</p>
                   </div>
                 </div>
               ))
              }
            </div>
            <Link to='/map' className="see_in_map yellow_btn">Xəritədə bax</Link>
        </div>
      </div>
    </SimpleBar>
  )
}
