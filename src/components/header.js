import React , {useState } from 'react'
import { Link , useNavigate } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import LogoWhite from '../img/logistika_white_logo.svg';
import {changeLanguage} from '../themes/utilts/changeLanguages-utilts';
import { useTranslation } from "react-i18next";
import useStyles from './styleComponent/styleSimplebar';

const pages = [
  {
    text: 'Ziyarətçi girişi',
    url: '/guest'
  },
  {
    text: 'İşləmə prinsipi',
    url: '/principle'
  },
  {
    text: 'Əlaqə',
    url: '/contact'
  }
]

const reglog = [
  {
    text: 'Daxil ol',
    url: '/login'
  },
  {
    text: 'Qeydiyyatdan keç',
    flag: true,
    url: '/registration'
  }
]

const languages = [
  {
    text: 'En',
    name: 'en',
    url: '/en'
  },
  {
    text: 'Ru',
    name: 'ru',
    url: '/ru'
  }
]

export default function Header  (){
  const navigate = useNavigate()
  const classes = useStyles();
  const [active , setActive] = useState(1);
  const {i18n} = useTranslation();
  const changeLang = (lang)=>{
    changeLanguage(lang , i18n);
  }
  return (
    <div>
      <AppBar className={`${classes.root}`} position="static">
          <div className="container">
            <div className="row">
              <Toolbar className={`${classes.toolbar} header-main-container`} disableGutters>
                <Link to="/" className={classes.button}>
                      <img src={LogoWhite} alt="" />
                </Link>
                <Box className='box-header-links'>
                  {pages.map((page , id) => (
                    <Link className="links" to={page.url} key={id}>
                      {page.text}
                    </Link>
                  ))}
                </Box>
                <Box className="reglog_box">
                  {reglog.map((page , id) => (
                    <button 
                      className={`reglogBtn ${page.flag ? 'fillBtn' : ''}`} 
                      to={page.url}
                      key={id}
                      onClick = {()=> navigate("registred"+page.url)}
                      >
                      {page.text}
                    </button>
                  ))}
                </Box>

                <Box className="languages_box">
                  {languages.map((page , id) => (
                    <button 
                      key={id}
                      id={id}
                      className={active === id ? "active" : undefined}
                      onClick={()=>{
                        changeLang(page.name)
                        setActive(id)
                        }}>
                      {page.text}
                    </button>
                  ))}
                </Box>

                <button className='hamburger-menu'>
                  <img src='/img/hamburger.svg' alt="" />
                </button>

              </Toolbar>
            </div>
          </div>
      </AppBar>
    </div>
  );
};
