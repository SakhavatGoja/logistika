import React from 'react'
import { Link, useNavigate } from 'react-router-dom';
import wp from '../img/wp.svg'
import insta from '../img/insta.svg'
import fb from '../img/fb.svg'
import ytb from '../img/ytb.svg'
import phone from '../img/phone.svg'
import loc from '../img/loc.svg'
import mail from '../img/mail.svg'
import goBack from '../img/goBack.svg'
import bigLoc from '../img/big_loc.svg'

const contactState = [
  {
    src: phone,
    text: '+99451 206 53 66'
  },
  {
    src: loc,
    text: "Bakı ş. Nərimanov ray. Ziya Bünyadov ev 2075–ci Məhəllə"
  },
  {
    src: mail,
    text: 'info@merlogistics.az'
  }
]

const socials = [
  {
    src : wp
  },
  {
    src : insta
  },
  {
    src : fb
  },
  {
    src : ytb
  }
]

export default function Contact() {
  const navigate = useNavigate();
  return (
    <div className="contact">
      <div className="contact_main_container">
        <div className="left_contact">
          <button className="goBack" onClick={()=> navigate(-1)}>
            <img src={goBack} alt="" />
            geri qayıt
          </button>
          <div className="contact_center">
            <p className="contact_title">Əlaqə</p>
            {contactState.map((item ,id)=>(
              <div className="contact_row" key={id}>
                <div className="con_img">
                  <img src={item.src} alt="" />
                </div>
                <p>{item.text}</p>
              </div>
            ))}
          </div>
          <div className="contact_social">
            {socials.map(({src} , id)=>(
              <Link to='/asdasd' key={id}>
                <img src={src}  alt="" />
              </Link>
            ))}
          </div>
        </div>
        <div className="right_contact">
          <button className="goBack" onClick={()=> navigate(-1)}>
            <img src={goBack} alt="" />
            geri qayıt
          </button>
          <div className="center_right">
            <p>Elan yerləşdir və özünün təyin etdiyi qiymətə yükünü daşıt</p>
            <img src={bigLoc} alt="" />
          </div>
        </div>
      </div>
      <div className="absolute_contact">
      </div>
    </div>
  )
}
