import React ,{useState} from 'react';
import XCart from '../xlibs/components/x-cart/XCart';
import XInput from '../xlibs/components/XInput';
import { Link } from 'react-router-dom';
import { Tabs , Tab } from 'react-bootstrap';
import Xleft from '../xlibs/components/Xleft';
import XSelect from '../xlibs/components/x-cart/XSelect';
import XRadioCart from '../xlibs/components/x-cart/XRadioCart';
import SimpleBar from 'simplebar-react';
import {loginAuth, signUp} from '../api/apiCall';
import 'simplebar/dist/simplebar.min.css';
import XFile from '../xlibs/components/x-cart/XFile';
import useInput from '../themes/utilts/useInput';
import Timer from '../themes/utilts/xtimer-utilts';
import Data from '../json/data.json';
import XModal from '../xlibs/components/x-cart/XModal';
import Registered from '../pages/registered/Registered';
import useStyles from './styleComponent/styleSimplebar';


const Registration = ({eventKey}) => {
  const classes = useStyles();
  const [state, setState] = useState({number:"", password :""});
  const [code , setCode] = useState(1);
  const [modalShow, setModalShow] = useState(false);
  const [title, setTitle] = useState('');


  const addBorder = (e)=>{
    e.target.value !== ''? e.target.classList.add('bordered') :e.target.classList.remove('bordered');
    e.target.value = e.target.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
  }

  const popUp = (e)=>{
    e.preventDefault();
    setModalShow(true);
    setTitle(Data.loadPop.titleSeller);
  }

  // const onChange = (e) => {
  //   e.preventDefault();
  //   resetFirstName();
  //   resetLastName();
  // }

  // const onChange = (e) =>{
  //   setState({...state , [e.target.name]:e.target.value }) 
  // }
  // const onSignUp = () => {
  //   signUp({number: state.number , password: state.password})
  //   loginAuth(state.number)
  // }
  // {
  //   <form onSubmit={onSignUp}>
  //     <input name="number" value={state.number} onChange ={(e)=>{onChange(e)}}/>
  //     <input name="password" value={state.password} onChange ={(e)=>{onChange(e)}}/>
  //     <button> submit</button>
  //   </form>
  // }

  return (
    <div className="reg_log_container">
      <Xleft 
        title='Elan yerləşdir və özünün təyin etdiyiyi qiymətə yükünü daşıd'
        imgStatusCode={code}
      />
      <SimpleBar className={classes.simplebar} >
        <div className="cart-container flex-column">
          {/* <Registered/>          */}
          <XCart
              title='Kodu daxil edin'
              className={'cart-right cart-otp'}
              Adornment = {
                (
                  <div className='otp_inner'>
                    <div className="otp_number_box">
                      <p className="left_otp">
                        Cod bu nömrəyə göndərildi:
                      </p>
                      <span className="number">+994 51 354 ** 89</span>

                    </div>
                    <button className='edit_number'>Nömrədə düzəliş et</button>
                    <form action="" className="otp_form">
                      <input type="text" pattern="[0-9]+" maxLength={1} onChange={(e)=>addBorder(e)}/>
                      <input type="text" pattern="[0-9]+" maxLength={1} onChange={(e)=>addBorder(e)}/>
                      <input type="text" pattern="[0-9]+" maxLength={1} onChange={(e)=>addBorder(e)}/>
                      <input type="text" pattern="[0-9]+" maxLength={1} onChange={(e)=>addBorder(e)}/>
                    </form>
                    <div className="otp_expire_box">
                      <div className="expire_heading">
                        <p className="title-expire">Bu kodun yararlılıq müddəti bitəcək</p>
                        <Timer 
                          initMinute={'00'} 
                          initSeconds={'05'}/>
                      </div>
                      <button className='resendOtp'>Yenidən göndər</button>
                    </div>
                  </div>
                )
              }
          />
          <XCart
            title="Qeydiyyat tipi1"
            className={'cart-right right-same-form-container'}
            Adornment = {
              (
                <form  className='standart_form third_form radio_form '>
                  <XRadioCart
                    type='radio'
                    name='type_1'
                    value='Beynəlxalq'
                    required
                  />
                  <XRadioCart
                    type='radio'
                    name='type_1'
                    value='Daxili'
                    required
                  />
                </form>
              )
            }
          />
          <XCart
            title="Qeydiyyat tipi2"
            className={'cart-right right-same-form-container'}
            Adornment = {
              (
                <form  className='standart_form third_form radio_form'>
                  <XRadioCart
                    type='radio'
                    name='type_2'
                    value='Daşıyıcı'
                    required
                  />
                  <XRadioCart
                    type='radio'
                    name='type_2'
                    value='Sifarişçi'
                    required
                  />
                </form>
              )
            }
          />
          <XCart
            title="Qeydiyyat tipi2.1"
            className={'cart-right right-same-form-container'}
            Adornment = {
              (
                <form  className='standart_form third_form radio_form'>
                  <XRadioCart
                    type='radio'
                    name='type_3'
                    value='Fiziki'
                    required
                  />
                  <XRadioCart
                    type='radio'
                    name='type_3'
                    value='Firma'
                    required
                  />
                </form>
              )
            }
          />
          <XCart
            title="Sifarişci qeydiyyatı3.2"
            className={'cart-right right-same-form-container'}
            Adornment = {
              (
                <form  className='standart_form third_form yellow_form'>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <select name="country" id="country" className='form-select'>
                        <option value="1">Azerbaijan</option>
                        <option value="2">Russia</option>
                        <option value="3">USA</option>
                        <option value="4">Georgia</option>
                        <option value="5">Sweden</option>
                      </select>  
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="number"
                        defaultValue=""
                        placeholder="Telefon nömrəniz"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Ad Soyad"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Şirkət adı"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="email"
                        defaultValue=""
                        placeholder="E-poçt"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="number"
                        defaultValue=""
                        placeholder="VÖEN"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Fəaliyyət kodu"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="password"
                        defaultValue=""
                        placeholder="Şifrə "
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="password"
                        defaultValue=""
                        placeholder="Şifrəni təkrarla"
                        required
                      />  
                    </div>
                  </div>
                  <XInput
                    className='input-submit input-submit-2'
                    type="submit"
                    value="Qeydiyyatı bitir"
                  />
                </form>
              )
            }
          />
          <XCart
            title="Sifarişci qeydiyyatı4.2"
            text="Sifariş vermək və ya götürmək üçün bu qeydiyyat vacibdir."
            className={'cart-right right-same-form-container-2'}
            Adornment = {
              (
                <form  className='standart_form third_form yellow_form'>
                  <XFile
                    placeholder='VÖEN şəkli'
                    name='voen'
                    id='voen'
                  />
                  <XFile
                    placeholder='Çıxarış şəkli'
                    name='image-1'
                    id='image-1'
                  />
                  <XFile
                    placeholder='Nizamnamə şəkli'
                    name='charter'
                    id='charter'
                  />
                  <XFile
                    placeholder='Direktor şəxsiyyət vəsiqəsinin şəkli (ön)'
                    name='director_front_img'
                    id='director_front_img'
                  />
                  <XFile
                    placeholder='Direktor şəxsiyyət vəsiqəsinin şəkli (arxa)'
                    name='director_back_img'
                    id='director_back_img'
                  />
                  <XFile
                    placeholder='Bank rekvisitlerinin şəkli'
                    name='bank'
                    id='bank'
                  />
                  <XInput
                    className='input-submit input-submit-2'
                    type="submit"
                    value="Qeydiyyatı bitir"
                  />
                </form>
              )
            }
          />
          <XCart
            title="Sürücü qeydiyyatı3.1"
            className={'cart-right right-same-form-container'}
            Adornment = {
              (
                <form  className='standart_form third_form yellow_form'>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <select name="country-1" id="country-1" className='form-select'>
                        <option value="1">Azerbaijan</option>
                        <option value="2">Russia</option>
                        <option value="3">USA</option>
                        <option value="4">Georgia</option>
                        <option value="5">Sweden</option>
                      </select>  
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="number"
                        defaultValue=""
                        placeholder="Telefon nömrəniz"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Ad Soyad"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="email"
                        defaultValue=""
                        placeholder="E-poçt"
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Maşın modeli"
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Maşın nömrəsi"
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Maşın növü"
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="text"
                        defaultValue=""
                        placeholder="Maşın ölçüləri"
                        required
                      />  
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="password"
                        defaultValue=""
                        placeholder="Şifrə "
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-container input-same">
                      <span>*</span>
                      <XInput
                        type="password"
                        defaultValue=""
                        placeholder="Şifrəni təkrarla"
                        required
                      />
                    </div>
                  </div>
                  <XInput
                    className='input-submit input-submit-2'
                    type="submit"
                    value="Qeydiyyatı bitir"
                  />
                </form>
              )
            }
          />
          <XCart
            title="Qeydiyyatı tamamla və sifariş et4.1-1"
            text="Sifariş vermək və ya götürmək üçün bu qeydiyyat vacibdir."
            className={'cart-right right-same-form-container-2'}
            Adornment = {
              (
                <form  className='standart_form third_form yellow_form'>
                  <XFile
                    placeholder='VÖEN şəkli'
                    name='voen-1'
                    id='voen-1'
                  />
                  <XFile
                    placeholder='Sürücülük vəsiqəsinin şəkli'
                    name='driving_license'
                    id='driving_license'
                  />
                  <XFile
                    placeholder='Xarici pasport şəkli'
                    name='foreign_passport'
                    id='foreign_passport'
                  />
                  <XFile
                    placeholder='Şəxsiyyət vəsiqəsinin şəkli (ön)'
                    name='identity_front_img'
                    id='identity_front_img'
                  />
                  <XFile
                    placeholder='Şəxsiyyət vəsiqəsinin şəkli (arxa)'
                    name='identity_back_img'
                    id='identity_back_img'
                  />
                  <XFile
                    placeholder='Texniki pasportların şəkli (ön)'
                    name='techPass_front_img'
                    id='techPass_front_img'
                  />
                  <XFile
                    placeholder='Texniki pasportların şəkli (arxa)'
                    name='techPass_back_img'
                    id='techPass_back_img'
                  />
                  <XFile
                    placeholder='Qeydiyyatlı olduğu firma sənədinin şəkli'
                    name='company_img'
                    id='company_img'
                  />
                  <XFile
                    placeholder='Qoşqunun lisenziyasının şəkli'
                    name='trailer'
                    id='trailer'
                  />
                  <XFile
                    placeholder='Bank rekvisitlerinin şəkli'
                    name='bank-1'
                    id='bank-1'
                  />
                  <XInput
                    className='input-submit input-submit-2'
                    type="submit"
                    value="Qeydiyyatı bitir"
                  />
                </form>
              )
            }
          />
           <XModal 
            title ={title} 
            data={Data.loadPop.data}
            show={modalShow}
            onHide={() => setModalShow(!modalShow)}
          />
        </div>
      </SimpleBar>
    </div>
  )
}

export default Registration