import React ,{useState} from 'react'
import SimpleBar from 'simplebar-react';
import { useNavigate } from 'react-router-dom';
import Data from '../json/data.json'
import useStyles from './styleComponent/styleSimplebar';

export default function Bid() {
  const [value, setValue] = useState(0)
  const classes = useStyles();
  const navigate = useNavigate();

  const decrease = () =>{
    if(value<1){
      return 0
    }
    setValue(value-1);
  }

  const increase = () =>{
    setValue(value+1);
  
  }
  return (
    <SimpleBar className={classes.simplebar}>
    <div className='notification_main_container'>
        <div className="head_notification">
          <button onClick={()=> navigate(-1)}>
            <img src="/img/left_breadcrump.svg" alt="" />
          </button>
          <p className="title_not">Bildirişlər</p>
        </div>
        <div className="bid_main_container">
          <div className="bid_scene">
            <div className="bid_left">
              <span>Ən yüksək təklif</span>
              <div className="highest_price">
                <p>{Data.bid.highPrice}</p>
                <img src="/img/manat.svg" alt="" />
              </div>
            </div>
            <button className='btn_blue'>Təkliflə razılaş</button>
          </div>
          <div className="bid_bottom_box">
            <div className="bid_list_box">
              <p className='title_bid'>Bütün təkliflər</p>
              <div className="bid_list">
                {
                  Data.bid.data.map((bid ,id)=>(
                    <div className="bid_row" key={id}>
                      <div className="bid_person_img">
                        <img src={bid.img} alt="" />
                      </div>
                      <div className="bid_person_data">
                        <p className="person_name">{bid.name}</p>
                        <p className="person_capacity">{bid.capacity}</p>
                      </div>
                      <div className="bid_right_box">
                        <div className={`status_bid ${bid.filter_status===1?'ascent':'descent' }`}>
                          <svg width="10" height="14" viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd" d="M4.99945 13.125C4.88342 13.125 4.77213 13.0789 4.69009 12.9969C4.60804 12.9148 4.56195 12.8035 4.56195 12.6875L4.56195 2.36863L1.8092 5.12225C1.72705 5.2044 1.61563 5.25055 1.49945 5.25055C1.38327 5.25055 1.27185 5.2044 1.1897 5.12225C1.10755 5.0401 1.0614 4.92868 1.0614 4.8125C1.0614 4.69632 1.10755 4.5849 1.1897 4.50275L4.6897 1.00275C4.73034 0.962007 4.77862 0.929683 4.83177 0.907627C4.88492 0.885571 4.9419 0.874219 4.99945 0.874219C5.05699 0.874219 5.11398 0.885571 5.16713 0.907627C5.22028 0.929683 5.26856 0.962007 5.3092 1.00275L8.8092 4.50275C8.89135 4.5849 8.9375 4.69632 8.9375 4.8125C8.9375 4.92868 8.89135 5.0401 8.8092 5.12225C8.72705 5.2044 8.61563 5.25055 8.49945 5.25055C8.38327 5.25055 8.27185 5.2044 8.1897 5.12225L5.43695 2.36863L5.43695 12.6875C5.43695 12.8035 5.39085 12.9148 5.30881 12.9969C5.22676 13.0789 5.11548 13.125 4.99945 13.125Z"  strokeWidth="0.4"/>
                          </svg>
                        </div>
                        <div className="bid_order">
                          <p>{bid.price}</p>
                          <img src="/img/manat.svg" alt="" />
                        </div>
                      </div>
                    </div>
                  ))
                }
              </div>
            </div>
            <div className="bid_estimate_box">
              <p className="estimate_title">Təklif ver</p>
              <div className="operation_box">
                <div className="operation_bid">
                  <input type="number" name="bid" id="bid" value={value} onChange={e => setValue(e.target.value)}/>
                  <img src="/img/manat.svg" alt="" />
                </div>
                <div className="operation_btns">
                  <button onClick={()=>decrease()}><img src="/img/minus.svg" alt="" /></button>
                  <button onClick={()=>increase()}><img src="/img/plus.svg" alt="" /></button>
                </div>
                <button className="bid_btn white_blue">
                  Təklifini ver
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </SimpleBar>
  )
}
