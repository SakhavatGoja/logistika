import React ,{useState} from 'react'
import SimpleBar from 'simplebar-react';
import { Link } from 'react-router-dom'
import Data from '../json/data.json'
import useStyles from './styleComponent/styleSimplebar';

const loads = true;
export default function Cars() {
  const classes = useStyles();

  return (
    <SimpleBar className={classes.simplebar}>
      {
        loads ? 
            <div className="history cars_container">
              <p className="history_title">Maşınların siyahısı</p>
                {
                  Data.cars.map((item,id)=>{
                    const {seriaNumber ,type ,size} = item.info[0];
                    return (
                      <div className="car-item" key={id}>
                        <p className="car-name">{item.name}</p>
                        <table>
                          <tbody>
                            <tr>
                              <th>Nömrəsi:</th>
                              <td>{seriaNumber}</td>
                            </tr>
                            <tr>
                              <th>Növü:</th>
                              <td>{type}</td>
                            </tr>
                            <tr>
                              <th>Ölçüləri:</th>
                              <td>{size}</td>
                            </tr>
                          </tbody>
                        </table>
                        <div className="car_truck_img">
                          <img src="/img/car_truck.png" alt="" />
                          <p className={item.free ? 'free' : 'busy'}>{item.free ? 'Boşdur': 'Məşğul'}</p>
                        </div>
                      </div>
                    )
                  })
                }
            </div>
              : <div className='none_broker'>
                  <p>{Data.loads.no[0].text}</p>
                  <button>{Data.loads.no[0].link}</button>
                </div>
      }
    </SimpleBar>
  )
}
