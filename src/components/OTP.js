import React ,{useState} from 'react';
import { Link } from 'react-router-dom';
import XCart from '../xlibs/components/x-cart/XCart';
import Xleft from '../xlibs/components/Xleft';
import Timer from '../themes/utilts/xtimer-utilts';

const OTP = () => {

  const addBorder = (e)=>{
    e.target.value !== ''? e.target.classList.add('bordered') :e.target.classList.remove('bordered');
    e.target.value = e.target.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
  }

  return (
    <div className="reg_log_container">
      <Xleft 
        title='Elan yerləşdir və özünün təyin etdiyiyi qiymətə yükünü daşıd'
        imgStatusCode={1}
      />
      <div className="cart-container">
        <XCart
            title='Kodu daxil edin'
            className={'cart-right cart-otp'}
            Adornment = {
              (
                <div className='otp_inner'>
                  <div className="otp_number_box">
                    <p className="left_otp">
                      Cod bu nömrəyə göndərildi:
                    </p>
                    <span className="number">+994 51 354 ** 89</span>

                  </div>
                  <Link to='/changeNumber' className='edit_number'>Nömrədə düzəliş et</Link>
                  <form action="" className="otp_form">
                    <input type="text" pattern="[0-9]+" maxLength={1} onChange={(e)=>addBorder(e)}/>
                    <input type="text" pattern="[0-9]+" maxLength={1} onChange={(e)=>addBorder(e)}/>
                    <input type="text" pattern="[0-9]+" maxLength={1} onChange={(e)=>addBorder(e)}/>
                    <input type="text" pattern="[0-9]+" maxLength={1} onChange={(e)=>addBorder(e)}/>
                  </form>
                  <div className="otp_expire_box">
                    <div className="expire_heading">
                      <p className="title-expire">Bu kodun yararlılıq müddəti bitəcək</p>
                      <Timer 
                        initMinute={'00'} 
                        initSeconds={'05'}/>
                    </div>
                    <button className='resendOtp'>Yenidən göndər</button>
                  </div>
                </div>
              )
            }
        />
      </div>
    </div>
  )
}

export default OTP