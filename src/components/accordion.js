import React from 'react'
import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';

const AccordionStyle = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  '&:not(:last-child)': {
    borderBottom: 0,
  },
  '&:before': {
    display: 'none',
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === 'dark'
      ? '#FCFCFC'
      : '#FCFCFC',
  flexDirection: 'row',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  borderBottom: '.5px solid #AEB5BC'
  }));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

export default function Accordion({title ,content ,index,expanded , handleChange}) {
  return (
    <div>
      <AccordionStyle expanded={expanded === `panel${index+1}`} onChange={handleChange}>
        <AccordionSummary  aria-controls={`panel${index+1}d-content`} id={`panel${index+1}d-header`}>
          {/* <Typography className="title_accordion"></Typography> */}
          <div className="accordion_title">{title}</div>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <span className="accordion_content">{content}</span>
          </Typography>
        </AccordionDetails>
      </AccordionStyle>
    </div>
  );
}