import React from 'react'
import LeftBar from './leftBar'
import SimpleBar from 'simplebar-react';
import useStyles from './styleComponent/styleSimplebar';
import { useNavigate } from 'react-router-dom';
import Data from '../json/data.json'



export default function NotificationInner() {
  const classes = useStyles();
  const navigate = useNavigate();
  return (
    <div className='main_theme_container'>
      <LeftBar/>
      <SimpleBar className={classes.simplebar}>
        <div className="notification_main_container">
          <div className="head_notification">
            <button onClick={()=> navigate(-1)}>
              <img src="/img/left_breadcrump.svg" alt="" />
            </button>
            <p className="title_not">Bildirişlər</p>
          </div>
          <div className="notification_inner_body">
            <p className="date_not">Dünən</p>
            <div className="nt_inner_heading_text">
              <p className="inner_head">Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet</p>
              <p className="inner_body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo,
                vel fringilla est ullamcorper eget nulla
              </p>
            </div>
            <div className="loads_table">
                {
                  Data.innerLoad.body.map((item,id)=>(
                    <div className="body_row" key={id}>
                      <p className="row_left">{item.head}</p>
                      <p className="row_right">{item.body}</p>
                    </div>
                  ))
                }
            </div>
            <button class="btn_blue">Qəbul et</button>
          </div>
        </div>
      </SimpleBar>
    </div> 
  )
}
