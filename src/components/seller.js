import React from 'react'
import SimpleBar from 'simplebar-react';
import useStyles from './styleComponent/styleSimplebar';
import Data from '../json/data.json'


const broker = true
export default function Seller() {
  const classes = useStyles();

  return (
    <SimpleBar className={classes.simplebar}>
      <div className='main_container'>
        {
          broker ? 
                <div className="broker_seller">
                  <p className="main_title">Satıcılar</p>
                  <div className="main_list">
                    {
                      Data.seller.yes.map((item,id)=>(
                        <div className="main_row" key={id}>
                          <div className="main_left_row">
                            <img src={item.avatar} alt="" className='circleImg'/>
                            <div className="naming_main">
                              <p className="name">{item.name}</p>
                              <p className="work">{item.work}</p>
                            </div>
                          </div>
                          <img src={item.country} alt="" className='circleImg'/>
                        </div>
                      ))
                    }
                  </div>
                </div> 
                : <div className='none_main'>
                    <p>{Data.seller.no[0].text}</p>
                    <button>{Data.seller.no[0].link}</button>
                  </div>
        }
      </div>
    </SimpleBar>
  )
}
