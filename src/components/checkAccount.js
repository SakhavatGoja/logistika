import React , {useState , useEffect} from 'react'
import {useNavigate} from 'react-router-dom';
import Xleft from '../xlibs/components/Xleft';
import XCart from '../xlibs/components/x-cart/XCart';
import XInput from '../xlibs/components/XInput';
import XSelect from '../xlibs/components/x-cart/XSelect';
import {getAllCountry, signUp} from '../api/apiCall'


const initialUser = {
  number : '',
  password: ''
}


export default function CheckAccount() {
  const [user, setUser] = useState(initialUser);
  const navigate = useNavigate()

  const [allCountries,setCountries] =useState([]);
  useEffect(()=> { 
    getAllCountry()
    .then(res=>setCountries(res.data.data))
   },[])
  
  return (
    <div className='reg_log_container'>
      <Xleft 
        title='Elan yerləşdir və özünün təyin etdiyiyi qiymətə yükünü daşıd'
        imgStatusCode={1}
      />
      <div className="container-right number-check-container w-100">
        <XCart
          strtext='Broker hesabını yoxla'
          text='Giriş etmək üçün telefon nömrənizi daxil edin'
          Adornment = {
              (
                <div className='first_step'>
                  <form className='standart_form form_first'>
                      <div className="form-group input-same">
                        <div className="input-container">
                          <XSelect
                            items={allCountries}
                          />
                          <XInput
                            type="number"
                            className=""
                            defaultValue=""
                            placeholder="Nomrenizi daxil edin"
                            required
                          />
                        </div>
                      </div>
                      <button
                        type="submit"
                        className="input-submit"
                        to={'/account-data'}
                        onClick = {()=> navigate("/account-data/inner")}
                        >
                         Hesabı yoxla
                      </button>
                  </form>
                </div>
              )
            }
        />
     </div>
    </div>
  )
}
